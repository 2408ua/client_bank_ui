import snackBar from './snackBar';
import drawer from './drawer';

const todo = {
  snackBar,
  drawer,
}
export default todo;
