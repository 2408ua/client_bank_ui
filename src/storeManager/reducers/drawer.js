import DashboardIcon from '@mui/icons-material/Dashboard';
import PeopleIcon from '@mui/icons-material/People';
import getKey from '../../helpers/cyptoRandomString';
import {DRAWER_SELECT_MAIN_LIST, DRAWER_OPEN} from "../actions/types";
import Dashboard from '../../layouts/dashboard/Dashboard';
import Customers from '../../layouts/customers/Customers';

const initialState = {
  open: true,
  mainListItems: [
    {
      name: 'Dashboard',
      icon: <DashboardIcon/>,
      component: <Dashboard/>,
      value: true,
      key: getKey()
    },
    {
      name: 'Customers',
      icon: <PeopleIcon/>,
      component: <Customers/>,
      value: false,
      key: getKey()
    }
  ],
  secondaryListItems: []
};

const state = (state = initialState, actions) => {
  const {payload, type} = actions;
  switch (type) {
    case DRAWER_OPEN:
      return {
        ...state,
        ...{open: payload},
      };
    case DRAWER_SELECT_MAIN_LIST:
      const mainListItems = state.mainListItems.map(v => {
        v.value = v.key === payload;
        return v;
      });
      return {
        ...state,
        ...{mainListItems},
      };
    default:
      return state;
  }
}

export default state;
