import {
  SHOW_SHACKBAR,
  DISABLE_SHACKBAR
} from '../actions/types'

const initialState = {
  variant: '',
  msg: '',
  show: false,
};
const todo = (state = initialState, actions) => {

  const {payload, type} = actions;

  switch (type) {
    case SHOW_SHACKBAR:
      return payload;
    case DISABLE_SHACKBAR:
      return payload;
    default:
      return state;
  }
}

export default todo;
