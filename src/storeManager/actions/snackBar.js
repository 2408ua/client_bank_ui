import {
  SHOW_SHACKBAR,
  DISABLE_SHACKBAR,
} from './types'


export const showSnackBar = (variant, msg) => dispatch => {
  dispatch({
    type: SHOW_SHACKBAR,
    payload: {
      variant,
      msg,
      show: true,
      loading: true,
    }
  });
};

export const disableSnackBar = () => dispatch => {
  dispatch({
    type: DISABLE_SHACKBAR,
    payload: {
      variant: '',
      msg: '',
      show: false,
      loading: false,
    }
  });
};