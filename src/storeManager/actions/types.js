//snack-bar
export const SHOW_SHACKBAR = 'SHOW_SHACKBAR';
export const DISABLE_SHACKBAR = 'DISABLE_SHACKBAR';

//driver
export const DRAWER_OPEN = 'DRAWER_OPEN';
export const DRAWER_SELECT_MAIN_LIST = 'DRAWER_SELECT_MAIN_LIST';
