import {
  DRAWER_SELECT_MAIN_LIST,
  DRAWER_OPEN,
} from './types'

export const drawerSelectMainList = (key) => dispatch => {
  dispatch({
    type: DRAWER_SELECT_MAIN_LIST,
    payload: key,
  });
};

export const toggleDrawer = (open) => dispatch => {
  dispatch({
    type: DRAWER_OPEN,
    payload: !open,
  });
};
