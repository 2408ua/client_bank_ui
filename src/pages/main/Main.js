import * as React from 'react';
import {createTheme, ThemeProvider} from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Container from '@mui/material/Container';
import AppBar from '../../components/appBar/AppBar'
import Drawer from '../../components/drawer/Drawer'
import {connect} from "react-redux";

const mdTheme = createTheme({});

const Main = ({drawerComponent}) => {
  return (
    <ThemeProvider theme={mdTheme}>
      <Box sx={{display: 'flex'}}>
        <CssBaseline/>
        <AppBar/>
        <Drawer/>
        <Box
          component="main"
          sx={{
            backgroundColor: (theme) =>
              theme.palette.mode === 'light'
                ? theme.palette.grey[100]
                : theme.palette.grey[900],
            flexGrow: 1,
            height: '100vh',
            overflow: 'auto',
          }}
        >
          <Toolbar/>
          <Container maxWidth="lg" sx={{mt: 4, mb: 4}}>
            {drawerComponent}
          </Container>
        </Box>
      </Box>
    </ThemeProvider>
  );
}

const mapStateToProps = state => ({
  drawerComponent: state.drawer.mainListItems.filter(v => v.value)[0].component,
})

export default connect(mapStateToProps)(Main);
