import cryptoRandomString from 'crypto-random-string';
const getKey = () => cryptoRandomString({length: 15});
export default getKey;
