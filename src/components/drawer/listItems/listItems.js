import * as React from 'react';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';

const MainListItems = ({mainList, drawerSelectMainList}) => {
  return (
    <div>
      {
        mainList.map(item => (
          <ListItem onClick={() => drawerSelectMainList(item.key)} key={item.key} selected={item.value} button>
            <ListItemIcon>
              {item.icon}
            </ListItemIcon>
            <ListItemText primary={item.name}/>
          </ListItem>
        ))
      }
    </div>
  );
}

export default MainListItems;
