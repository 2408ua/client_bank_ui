import React from 'react';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import SnackBar from './layouts/snackBar/snackBar';
import Main from './pages/main/Main';

//Redux
import {Provider} from 'react-redux';
import store from './storeManager/store';

//CSS
import './app.scss';

function App() {
  return (
    <Provider store={store}>
      <Router>
        <SnackBar/>
        <Routes>
          <Route exact path={'/'} element={<Main/>}/>
        </Routes>
      </Router>
    </Provider>
  );
}

export default App;
