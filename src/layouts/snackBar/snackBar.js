import React from 'react';
import { connect } from 'react-redux';
import { SnackbarProvider, useSnackbar } from 'notistack';
import { disableSnackBar } from '../../storeManager/actions/snackBar';
import PropTypes from 'prop-types';

function MyApp({ variant, msg, show, disableSnackBar }) {
  const { enqueueSnackbar } = useSnackbar();

  if(show) enqueueSnackbar(msg, { variant });

  disableSnackBar();

  return <></>;
}

const IntegrationNotistack = ({ variant, msg, show, disableSnackBar }) => {
  return (
    <SnackbarProvider maxSnack={5}>
      <MyApp variant={variant} msg={msg} show={show} disableSnackBar={disableSnackBar}/>
    </SnackbarProvider>
  );
};

const mapStateToProps = state => ({
  variant: state.snackBar.variant,
  msg: state.snackBar.msg,
  show: state.snackBar.show,
});

IntegrationNotistack.propTypes = {
  disableSnackBar: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, { disableSnackBar })(IntegrationNotistack);
